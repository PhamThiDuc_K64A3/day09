<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        session_start();
        $question = array(1 => "1 + 1 = ?", 2 => "2 + 8 = ?",
        3 => "4 + 15 = ?", 4 => "9 + 8 = ?",
        5 => "6 + 7 = ?", 6 => "8 + 2 = ?",
        7 => "8 + 0 = ?", 8 => "4 - 2 = ?",
        9 => "7 - 6 = ?", 10 => "5 - 2 = ?");

        $answer = array(1 => array("0"=>0, "3"=>0, "2"=>1, "4"=>0), 2 => array("5"=>0, "10"=>1, "8"=>0, "2"=>0),
        3 => array("4"=>0, "15"=>0, "45"=>0, "19"=>1), 4 => array("54"=>0, "17"=>1, "5"=>0, "0"=>0),
        5 => array("13"=>1, "3"=>0, "1"=>0, "7"=>0), 6 => array("10"=>1, "0"=>0, "1"=>0, "2"=>0),
        7 => array("0"=>0, "8"=>1, "10"=>0, "9"=>0), 8 => array("4"=>0, "3"=>0, "2"=>1, "1"=>0),
        9 => array("7"=>0, "1"=>1, "6"=>0, "0"=>0), 10 => array("2"=>0, "4"=>0, "5"=>0, "3"=>1) );

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $_SESSION["answer"] = $answer;
            $_SESSION["question"] = $question;
            for ($i=1; $i <= 5; $i++) { 
                $key = "question_".strval($i);
                $_SESSION[$key]=$_POST[$key];
            }
            header("Location: quiz2.php");
        }
    ?>
    <form method="POST" enctype="multipart/form-data" action="<?php 
         echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <h1> Trắc nghiệm</h1>
        <?php
            foreach ($question as $key => $value) {
                if ($key <= 5){
                    echo "<p>Câu {$key}: {$value}</p>";
                    foreach ($answer as $key_ans => $value_ans) {
                        if ($key == $key_ans){
                            foreach ($value_ans as $vlue => $dan){
                                echo "<input type=\"radio\" name=\"question_{$key_ans}\" value=\"{$vlue}\"> {$vlue}<br>";
                            }
                        }
                    }
                }
            };
        ?>
        <button>Next</button>
    </form>
</body>
</html>