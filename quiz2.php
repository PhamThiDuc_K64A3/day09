<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        session_start();
        $question = $_SESSION["question"];
        $answer = $_SESSION["answer"];
        $page_1 = $_SESSION;
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $_SESSION = $page_1;
            for ($i=6; $i <= 10; $i++) { 
                $key = "question_".strval($i);
                $_SESSION[$key]=$_POST[$key];
            }
            header("Location: submit.php");
        }
    ?>
    <form method="POST" enctype="multipart/form-data" action="<?php 
         echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <h1> Trắc nghiệm</h1>
        <?php
            foreach ($question as $key => $value) {
                if ($key >= 6){
                    echo "<p>Câu {$key}: {$value}</p>";
                    foreach ($answer as $key_ans => $value_ans) {
                        if ($key == $key_ans){
                            foreach ($value_ans as $vlue => $dan){
                                echo "<input type=\"radio\" name=\"question_{$key_ans}\" value=\"{$vlue}\"> {$vlue}<br>";
                            }
                        }
                    }
                }
            };
        ?>
        <button>Nộp bài</button>
    </form>
</body>
</html>